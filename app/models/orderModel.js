// khai báo thư viên mongoose
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    _id: Schema.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },

    pizzaSize: {
        type: String,
        require: true
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: {
        type: Schema.Types.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: Schema.Types.ObjectId,
        ref: "Drink"
    },
    status: {
        type: String,
        require: true
    },

})

module.exports = mongoose.model("Order", orderSchema)